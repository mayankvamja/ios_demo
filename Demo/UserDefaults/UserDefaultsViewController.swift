//
//  UserDefaultsViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 09/03/21.
//

import UIKit

struct Person : Codable {
    var firstName: String
    var lastName: String
    var age: Double
}


class UserDefaultsViewController: UIViewController {
    
    var defaults = UserDefaults.standard
    let dataFilePath =
        FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        ).first?
        .appendingPathComponent("UserData")

    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var ageStepper: UIStepper!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    
    @IBOutlet var dataLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDataFromUserDefaults()
        loadDataFromPList()
        
    }

    @IBAction func onAgeChanged(_ sender: UIStepper) {
        ageLabel.text = "Age: \(Int(sender.value))"
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        let firstName = self.firstNameTextField.text ?? ""
        let lastName = self.lastNameTextField.text ?? ""
        let age = self.ageStepper.value
        
        let person = Person(firstName: firstName, lastName: lastName, age: age)
        
//        defaults.set(person, forKey: "UserDefaults_Data")
        defaults.set(firstName, forKey: "UserDefaults_FirstName")
        defaults.set(lastName, forKey: "UserDefaults_LastName")
        defaults.set(age, forKey: "UserDefaults_Age")
        
        let encoder = PropertyListEncoder()
        let data = try? encoder.encode(person)
        try? data?.write(to: dataFilePath!)
        
    }
    
    @IBAction func deleteClicked(_ sender: Any) {
        
        defaults.removeObject(forKey: "UserDefaults_FirstName")
        defaults.removeObject(forKey: "UserDefaults_LastName")
        defaults.removeObject(forKey: "UserDefaults_Age")

        let data = Data()
        try? data.write(to: dataFilePath!)
        
    }
    
    func loadDataFromUserDefaults() {
        if let firstName = defaults.string(forKey: "UserDefaults_FirstName"), let lastName = defaults.string(forKey: "UserDefaults_LastName") {
            
            let age = defaults.double(forKey: "UserDefaults_Age")
            ageStepper.value = age
            ageLabel.text = "Age: \(age)"
            lastNameTextField.text = lastName
            firstNameTextField.text = firstName
            
            dataLabel.text = """
                First name : \(firstName)
                Last name  : \(lastName)
                Age        : \(age)
            """
        }
    }
    
    func loadDataFromPList() {
        if let data = try? Data(contentsOf: dataFilePath!) {
            let decoder = PropertyListDecoder()
            if let person = try? decoder.decode(Person.self, from: data) {
                dataLabel.text = """
                    \(dataLabel.text!)

                    First name : \(person.firstName)
                    Last name  : \(person.lastName)
                    Age        : \(person.age)
                """
            }
        }
    }
}
