//
//  BrowserViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 08/04/21.
//

import UIKit
import WebKit

class BrowserViewController: UIViewController,UIWebViewDelegate{
    
    @IBOutlet var webView : WKWebView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var backWebpageButton: UIButton!
    @IBOutlet weak var nextWebpageButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    
    var loadingIndicator = UIActivityIndicatorView()
    var refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadingIndicator.center = self.view.center
        loadingIndicator.style = .large
        view.addSubview(loadingIndicator)
        webView.loadUrl(string: "https://google.com")
        webView.scrollView.refreshControl = refreshControl
        
        webView.navigationDelegate = self
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)

    }
    
    @IBAction func btnSearchAction(_ sender: UIButton) {

        if let urlString = searchTextField.text{
            if urlString.starts(with: "http://") || urlString.starts(with: "https://") {
                webView.loadUrl(string: urlString)
            } else if urlString.contains(".") || !urlString.contains(" ") {
                webView.loadUrl(string: "http://\(urlString)")
            } else {
                let textComponent = urlString.components(separatedBy: " ")
                let searchString = textComponent.joined(separator: "+")
                let url = "https://www.google.com/search?q=" + searchString
                webView.loadUrl(string: url)
            }
        }
    }
    
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        if webView.canGoBack { webView.goBack() }
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if webView.canGoForward { webView.goForward() }
    }
    
    @objc func handleRefreshControl() {
        webView.reload()
    }

}

extension BrowserViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation: WKNavigation!) {
        loadingIndicator.startAnimating()
        loadingIndicator.isHidden = false
        searchTextField.text = webView.url?.absoluteString
    }
    
    func webView(_ webView: WKWebView, didFinish: WKNavigation!) {
    
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        loadingIndicator.stopAnimating()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    func webView(_ webView: WKWebView, didFail: WKNavigation!, withError: Error) {
        loadingIndicator.stopAnimating()
    }


}

extension WKWebView {
    func loadUrl(string: String) {
        if let url = URL(string: string) {
            load(URLRequest(url: url))
        }
    }
}

