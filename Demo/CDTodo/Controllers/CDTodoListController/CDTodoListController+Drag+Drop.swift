//
//  TodoListController+Drag+Drop.swift
//  Demo
//
//  Created by Mayank Vamja on 08/03/21.
//

import UIKit
import MobileCoreServices

extension CDTodoListController: UITableViewDragDelegate {
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let src = sourceIndexPath.row
        let dst = destinationIndexPath.row
        
        if src == dst { return }
        for x in self.todos {
            print(x.text!)
        }
        
        self.todos[src].index = self.todos[dst].index
        
        
        if src > dst {
            for i in dst...(src - 1) {
                self.todos[i].index = self.todos[i].index + 1
            }
        } else {
            for i in (src + 1)...dst {
                self.todos[i].index = self.todos[i].index - 1
            }
        }
        
        self.container.save {
            self.loadItems()
        }
        
    }
    
    // MARK: UITableViewDragDelegate
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        guard let data = todos[indexPath.row].text?.data(using: .utf8) else { return [] }
        
        let itemProvider = NSItemProvider(item: data as NSData, typeIdentifier: kUTTypePlainText as String)

        return [UIDragItem(itemProvider: itemProvider)]
    }
    
    /* // MARK: UITableViewDropDelegate
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        print("PERFORM DROP")
    } */
}
