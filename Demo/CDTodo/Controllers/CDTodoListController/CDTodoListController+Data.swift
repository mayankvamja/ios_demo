//
//  CDTodoListController+Data.swift
//  Demo
//
//  Created by Mayank Vamja on 10/03/21.
//

import UIKit

extension CDTodoListController {
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cd_todo_item", for: indexPath) as! CDTodoCell

        cell.configure(with: todos[indexPath.row])
        cell.onStatusChange = { [unowned self] in
            self.todos[indexPath.row].done = !self.todos[indexPath.row].done
            self.container.save { self.loadItems() }
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedTodo = todos[indexPath.row]
        self.performSegue(withIdentifier: "update_todo_segue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    func makeSwipeToDeleteConfiguration(_ indexPath: IndexPath) -> UISwipeActionsConfiguration {
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            
            for i in indexPath.row..<self.todos.count {
                self.todos[i].index = self.todos[i].index - 1
            }
            
            self.container.context.delete(self.todos[indexPath.row])
            self.container.save {
                self.loadItems()
            }
            
            completionHandler(true)
        }
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let todo = todos[indexPath.row]
        
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "Options", children: [
                UIAction(title: "Set Notification") { [self] _ in
                    makeLocalNotification(list: selectedList!, todo: todo)
                },
                UIAction(title: "Share") { [self] _ in
                    shareTodoItem(list: selectedList!, todo: todo)
                },
            ])
        }
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

// MARK: ContextMenuAction Handlers
extension CDTodoListController {
    
    func shareTodoItem(list: CDTodoListItem, todo: CDTodoItem) {
        var items = [Any]()
        
//        items.append("lorem ipsum")
//        items.append("lorem ipsum")
//        items.append("lorem ipsum")
//        items.append(UIImage(named: "AppIcon")!)
//        items.append(URL(string: "https://newsoasis.herokuapp.com")!)
        
        if let date = todo.date {
            items.append("\(list.title!): \(todo.text!) on \(date)")
        } else {
            items.append("\(list.title!): \(todo.text!)")
        }
        
        let vc = UIActivityViewController(activityItems: items, applicationActivities: nil)
        
        vc.excludedActivityTypes = [.message]
        vc.completionWithItemsHandler = {
            (activityType, completed, arrayReturnedItems, error) in
            if completed {
                print("share completed")
                return
            } else {
                print("cancel")
            }
            if let shareError = error {
                print("error while sharing: \(shareError.localizedDescription)")
            }
        }
        
        present(vc, animated: true)
    }

    func makeLocalNotification(list: CDTodoListItem, todo: CDTodoItem) {
        
        let content = UNMutableNotificationContent()
        content.categoryIdentifier = "CDTodoItemNotifications"
        content.title = list.title!
        content.body = todo.text!
        content.sound = .default
        content.badge = 1
        content.userInfo = ["CDTodoListItem": list.title!, "CDTodoItemId": todo.index]
        
        var interval = 10.0
        
        if let date = todo.date {
            let remainingTime = date.timeIntervalSinceNow
            if remainingTime > 0 {
                interval = remainingTime
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            print("Notification added")
            if let error = error {
                print(error.localizedDescription)
            }
        }
        
    }
}
