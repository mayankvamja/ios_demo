//
//  CDTodoListControllerTableViewController.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 10/03/21.
//

import UIKit
import CoreData
import UserNotifications

class CDTodoListController: UITableViewController {
    
    var selectedList: CDTodoListItem? {
        didSet {
            title = selectedList?.title ?? "List"
            if selectedList != nil {
                loadItems()
            }
        }
    }
    var selectedListName: String? {
        didSet {
            let request: NSFetchRequest<CDTodoListItem> = CDTodoListItem.fetchRequest()
            request.predicate = NSPredicate(format: "title MATCHES %@", selectedListName!)
            selectedList = try? container.context.fetch(request).first
            
            title = selectedListName ?? "List"
        }
    }
    var indexFromNotification: Int?
    
    
    var selectedTodo: CDTodoItem?
    var todos: [CDTodoItem] = []
    let container = PersistentContainer()
    let notificationCenter = UNUserNotificationCenter.current()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dragInteractionEnabled = true
        tableView.dragDelegate = self
        
    }
    
    func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            if let index = self.indexFromNotification {
                
                let indexPath = IndexPath(row: index, section: 0)
                self.indexFromNotification = nil
                
                self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
            }
        }
    }
    
    func loadItems(with request: NSFetchRequest<CDTodoItem> = CDTodoItem.fetchRequest(), predicate: NSPredicate? = nil) {
        
        let listPredicate = NSPredicate(format: "parentList = %@", selectedList!)
        
        if let predicate = predicate {
            request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [listPredicate, predicate])
        } else {
            request.predicate = listPredicate
        }
        
        request.sortDescriptors = [NSSortDescriptor(key: "index", ascending: true)]
        
        do {
            todos = try container.context.fetch(request)
            reloadTable()
        } catch {
            print("Unable to load context : \(error)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "update_todo_segue":
            if let updateTodoVC = segue.destination as? CDViewTodoController {
                updateTodoVC.configureAsUpdateView(selectedTodo)
                updateTodoVC.onUpdateTodo = { (todo: CDTodoItem) in
                    self.container.save { self.loadItems() }
                }
                selectedTodo = nil
            }
            
        case "add_todo_segue":
            if let addTodoVC = segue.destination as? CDViewTodoController {
                addTodoVC.onAddTodo = { (text: String, date: Date?) in
                    let newTodo = CDTodoItem(context: self.container.context)
                    newTodo.index = Int16(self.todos.count)
                    newTodo.text = text
                    newTodo.date = date
                    newTodo.parentList = self.selectedList
                    self.todos.append(newTodo)
                    self.container.save { self.loadItems() }
                }
            }
            
        default:
            return
        }
    }
}
