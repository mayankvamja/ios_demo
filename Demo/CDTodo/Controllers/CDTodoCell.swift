//
//  TodoCell.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class CDTodoCell: UITableViewCell {

    @IBOutlet var todoTextLabel: UILabel!
    @IBOutlet var todoCheckButton: UIButton!
    @IBOutlet var todoEditButton: UIButton!
    @IBOutlet var todoDateLabel: UILabel!
    var todo: CDTodoItem?
    var onStatusChange: () -> Void = {}
    
    func configure(with todo: CDTodoItem) {
        todoTextLabel.text = todo.text?.replacingOccurrences(of: "\n", with: " ")
        setDateLabel(from: todo.date)
        todoCheckButton.setImage(getCheckButtonImage(todo.done), for: .normal)
        
        self.todo = todo
    }
    
    func setDateLabel(from date: Date?) {
        if let date = date {
            let df = DateFormatter()
            df.dateFormat = "MMM d, y 'at' hh:mm aa"
            todoDateLabel.text = df.string(from: date)
            return
        }
        todoDateLabel.text = nil
    }
    
    func getCheckButtonImage(_ done: Bool) -> UIImage? {
        return done
            ? UIImage(systemName: "checkmark.square.fill")
            : UIImage(systemName: "square")
    }
    
    @IBAction func checkButtonClicked() {
        todoCheckButton.setImage(getCheckButtonImage(!todo!.done), for: .normal)
        self.onStatusChange()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
