//
//  ViewController.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit
import CoreData

class CDTodoListItemCell: UITableViewCell {
    @IBOutlet var view: UIView!
    @IBOutlet var label: UILabel!
}

class CDTodosHomeController: UITableViewController {

    let container = PersistentContainer()
    var lists: [CDTodoListItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isFirstResponder {
            print("VIEW CONTROLLER IS FIRST RESPONDER")
        }
        
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        
        loadItems()
        
    }
    
    // MARK: UITableView Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "view_todo_list_segue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToEditConfiguration(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cd_todo_list_name_cell")! as! CDTodoListItemCell
        cell.label.text = lists[indexPath.row].title
        
        let view = cell.view!
        view.layer.cornerRadius = 20.0
        view.layer.shadowRadius = 4.0
        view.layer.shadowOffset = .zero
        view.layer.shadowColor = UIColor.systemGray4.cgColor
        view.layer.shadowOpacity = 1

        return cell
    }
    
    
    // MARK: Load Items Method
    func loadItems(with request: NSFetchRequest<CDTodoListItem> = CDTodoListItem.fetchRequest()) {
        
        do {
            lists = try container.context.fetch(request)
            self.tableView.reloadData()
        } catch {
            print("Unable to load context : \(error)")
        }
    }
}

// MARK: UITableViewCell Swipe
extension CDTodosHomeController {
    func makeSwipeToDeleteConfiguration(_ indexPath: IndexPath) -> UISwipeActionsConfiguration {

        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            
            self.container.context.delete(self.lists[indexPath.row])
            self.container.save {
                self.loadItems()
            }
            
            completionHandler(true)
        }
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func makeSwipeToEditConfiguration(_ indexPath: IndexPath) -> UISwipeActionsConfiguration {

        let editAction = UIContextualAction(style: .destructive, title: nil) { [unowned self] (_, _, completionHandler) in
            
            var textField: UITextField!
            
            let alert = UIAlertController(title: "Edit name", message: "", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "Update Item", style: .default) { action in
                if let newTitle = textField.text {
                    self.lists[indexPath.row].title = newTitle
                    
                    self.container.save {
                        self.loadItems()
                    }
                }
            }
            
            alert.addTextField { alertTextField in
                alertTextField.text = self.lists[indexPath.row].title
                textField = alertTextField
            }
            
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
            completionHandler(true)
        }
        
        editAction.image = UIImage(systemName: "pencil")
        editAction.backgroundColor = .systemGreen
        
        return UISwipeActionsConfiguration(actions: [editAction])
    }
}

// MARK: UISearchBar Methods
extension CDTodosHomeController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count == 0 {
            loadItems()
            return
        }
        
        let request: NSFetchRequest<CDTodoListItem> = CDTodoListItem.fetchRequest()
        
        request.predicate = NSPredicate(format: "title CONTAINS[cd] %@", searchText)
        request.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        loadItems(with: request)
        
    }
}

// MARK: BarButtons
extension CDTodosHomeController {

    @IBAction func onAddButtonClicked(_ sender: Any) {
        
        var textField: UITextField!
        
        let alert = UIAlertController(title: "Add new todo list", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .destructive) { action in
            
            if textField.text?.count == 0 { return }
            
            let newItem = CDTodoListItem(context: self.container.context)
            newItem.title = textField.text!
            
            self.lists.append(newItem)
            self.container.save {
                self.loadItems()
            }
        }
        
        alert.addTextField { alertTextField in
            alertTextField.placeholder = "Type todo list name"
            textField = alertTextField
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onBackPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Prepare Method
extension CDTodosHomeController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "view_todo_list_segue" {
            let vc = segue.destination as! CDTodoListController
            
            if let indexPath = tableView.indexPathForSelectedRow {
                vc.selectedList = lists[indexPath.row]
            }
        }
    }
}
