//
//  CDViewTodoController.swift
//  Demo
//
//  Created by Mayank Vamja on 10/03/21.
//

import UIKit

class CDViewTodoController: UIViewController, UITextViewDelegate {
    
    @IBOutlet var todoTextView: UITextView!
    @IBOutlet var spacerView: UIStackView!
    @IBOutlet var charCountLabel: UILabel!
    @IBOutlet var dateEnableSwitch: UISwitch!
    @IBOutlet var todoDateTime: UIDatePicker!
    @IBOutlet var saveButton: UIBarButtonItem!

    var todoText: String = ""
    
    var isNewTodo: Bool = true
    var defaultTodo: CDTodoItem?
    
    var onAddTodo: (String, Date?) -> Void = { _,_  in }
    var onUpdateTodo: (CDTodoItem) -> Void = { _ in }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        todoTextView.becomeFirstResponder()
        
        todoTextView.delegate = self
        updateUI()
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (note) in
            guard let keyboardFrame = (note.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                self.additionalSafeAreaInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.size.height, right: 0.0)
            }, completion: nil)
        }
        
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (_) in
            UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                self.additionalSafeAreaInsets = .zero
            }, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func updateUI() {
        if let todo = defaultTodo {
            navigationItem.title = "Update"
            saveButton.isEnabled = true
            todoTextView.text = todo.text
            todoText = todo.text!
            charCountLabel.text = "\(todo.text!.count) / 500"
            if let date = todo.date {
                dateEnableSwitch.isOn = true
                todoDateTime.date = date
                todoDateTime.isHidden = false
            }
        } else {
            todoDateTime.minimumDate = Date()
        }
    }
    
    func configureAsUpdateView(_ todoItem: CDTodoItem?) {
        isNewTodo = false
        self.defaultTodo = todoItem
    }
    
    func validateTodo(_ newValue: String) {
        let value = newValue.trimmingCharacters(in: .whitespaces)
        if value.count == 0 {
            self.todoText = value
            self.saveButton.isEnabled = false
        }
        else {
            self.saveButton.isEnabled = true
            if value.count > 500 {
                self.todoText = String(value.prefix(500))
                self.todoTextView.text = self.todoText
            } else {
                self.todoText = value
            }
        }
        
        let count: Int = self.todoText.count
        charCountLabel.text = "\(count) / 500"
    }

    func textViewDidChange(_ textView: UITextView) {
        
        let textViewHeight = textView.frame.size.height
        let contentHeight = textView.contentSize.height
        let spacerViewHeight = spacerView.frame.size.height
        textView.isScrollEnabled = spacerViewHeight == 0 && contentHeight >= textViewHeight
        
        self.validateTodo(textView.text ?? "")
    }
    
    @IBAction func onDatePickerEnabled(_ sender: UISwitch) {
        todoDateTime.isHidden = !sender.isOn
    }
    
    @IBAction func onTodoDateChanged(_ sender: UIDatePicker) {
        print(sender.date)
    }
    
    @IBAction func onSaveClicked(_ sender: UIBarButtonItem) {
        let todoDate = dateEnableSwitch.isOn ? todoDateTime.date : nil
        
        
        if isNewTodo {
            self.onAddTodo(todoText, todoDate)
        } else {
            defaultTodo!.text = todoText
            defaultTodo!.date = todoDate
            self.onUpdateTodo(defaultTodo!)
        }

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCalcelClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
