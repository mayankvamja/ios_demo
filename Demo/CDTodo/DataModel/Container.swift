//
//  Container.swift
//  Demo
//
//  Created by Mayank Vamja on 11/03/21.
//

import UIKit
import CoreData

class PersistentContainer {
    
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func save(onSave: () -> Void) {
        
        context.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
        
        guard self.context.hasChanges else { return }
        
        do {
            try self.context.save()
            onSave()
        } catch {
            print("Error saving context: \(error.localizedDescription)")
        }
    }

}
