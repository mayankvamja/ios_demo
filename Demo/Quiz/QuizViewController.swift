//
//  ViewController.swift
//  Quizzler
//
//  Created by Mayank Vamja
//

import UIKit

class QuizViewController: UIViewController {
    
    @IBOutlet var scoreText: UILabel!
    @IBOutlet var questionText: UILabel!
    @IBOutlet var optionButtonA: UIButton!
    @IBOutlet var optionButtonB: UIButton!
    @IBOutlet var optionButtonC: UIButton!
    @IBOutlet var optionButtonD: UIButton!
    @IBOutlet var progressBar: UIProgressView!
    
    var quiz = Quiz()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateQuestionUI()
    }

    @IBAction func optionChoosed(_ sender: UIButton) {
        
        if quiz.isCorrect(sender.tag) {
            sender.backgroundColor = UIColor.green
        } else {
            sender.backgroundColor = UIColor.red
        }
        
        
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { [weak self] timer in
            self?.quiz.update()
            self?.updateQuestionUI()
        }
        
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
    }
    
    func updateQuestionUI() {
        let question = quiz.currentQuestion
        
        questionText.text = "\(question.label)"
        
        optionButtonA.setTitle(question.options[0], for: .normal)
        optionButtonB.setTitle(question.options[1], for: .normal)
        optionButtonC.setTitle(question.options[2], for: .normal)
        optionButtonD.setTitle(question.options[3], for: .normal)
        
        progressBar.progress = quiz.progress
        scoreText.text = "Score: \(quiz.score)"
        
        optionButtonA.backgroundColor = UIColor.clear
        optionButtonB.backgroundColor = UIColor.clear
        optionButtonC.backgroundColor = UIColor.clear
        optionButtonD.backgroundColor = UIColor.clear
    }
    
}

