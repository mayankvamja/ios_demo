//
//  Quiz.swift
//  Quizzler
//
//  Created by Mayank Vamja on 02/03/21.
//  Copyright © 2021 The App Brewery. All rights reserved.
//

import Foundation

struct Quiz {
    let questions: [Question] = [
        Question("1 + 2 is equal to?", options: ["1", "2", "3", "4"], answer: 2),
        Question("1 * 0 is equal to?", options: ["1", "Infinite", "-1", "0"], answer: 3),
        Question("1 / 0 is equal to?", options: ["1", "Infinite", "-1", "0"], answer: 1),
        Question("3 ^ 0 is equal to?", options: ["1", "2", "3", "4"], answer: 0)
    ]
    
    var currentQuestionNumber: Int = 0
    var currentQuestion: Question {
        return questions[self.currentQuestionNumber]
    }
    
    var progress: Float = 0.0
    var score = 0
    
    mutating func update() {
        self.currentQuestionNumber += 1
        if self.currentQuestionNumber >= questions.count {
            self.currentQuestionNumber = 0
        }
        
        self.progress = Float(self.currentQuestionNumber) / Float(self.questions.count)
    }
    
    mutating func isCorrect(_ answer: Int) -> Bool {
        if self.currentQuestion.isCorrect(answer) {
            self.score += 1
            return true
        }
        return false
    }
}
