//
//  Question.swift
//  Quizzler
//
//  Created by Mayank Vamja on 02/03/21.
//

import Foundation

struct Question {
    var label: String
    var options: [String]
    private var answer: Int
    
    init(_ label: String, options: [String], answer: Int) {
        self.label = label
        self.options = options
        self.answer = answer
    }
    
    func isCorrect(_ answer: Int) -> Bool {
        return answer == self.answer
    }
}
