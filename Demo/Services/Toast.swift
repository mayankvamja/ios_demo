//
//  Toast.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit

extension UIViewController {
    func showToast(message: String, seconds: Double, completion: @escaping ()->Void = {}) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alert.view.layer.cornerRadius = 15
        present(alert, animated: false)
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            alert.dismiss(animated: false)
            completion()
        }
    }
}
