//
//  LocaltionService.swift
//  Demo
//
//  Created by Mayank Vamja on 20/03/21.
//

import CoreLocation

class LocationService: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationService()
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation? = nil
    
    private override init() {
        super.init()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
    }
    
    func startMySignificantLocationChanges() {
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            // The device does not support this service.
            return
        }
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.activityType = .other
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func stopMySignificantLocationChanges() {
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        print(manager.authorizationStatus.rawValue <= 2 ? "Don't have Permission" : "Has location access permission")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            print("\(location.coordinate.latitude), \(location.coordinate.longitude)")
            currentLocation = location
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            print("Location permission is unavailable.")
            manager.stopMonitoringSignificantLocationChanges()
            return
        }
        print(error.localizedDescription)
    }
}
