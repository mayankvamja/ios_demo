//
//  ContactsService.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//


import Contacts

class ContactsService {
    
    static let store = CNContactStore()
    
    static func requestPermission() {
        store.requestAccess(for: .contacts) { granted, err in
            if granted {
                print("Contacts usage permission granted")
            } else {
                print("No Contacts Usage Permission \(err?.localizedDescription ?? "Unknown error")")
            }
        }
    }
    
    let keysToFetch = [
        CNContactGivenNameKey,
        CNContactFamilyNameKey,
        CNContactEmailAddressesKey,
        CNContactImageDataKey,
        CNContactPhoneNumbersKey,
        CNContactIdentifierKey
    ] as [CNKeyDescriptor]
    
    func fetchAllContacts() -> [MyContact] {
        
        var contacts: [MyContact] = []
        do {
            
            //let predicate = CNContact.predicateForContacts(matchingName: "Appleseed")
            //let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keysToFetch)
            
            let request = CNContactFetchRequest(keysToFetch: keysToFetch)
            try ContactsService.store.enumerateContacts(with: request) { (contact, _) in
                contacts.append(MyContact(contact: contact))
            }
            
        } catch {
            print("Failed to fetch contact, error: \(error.localizedDescription)")
        }
        
        return contacts
    }
    
    enum ContactSaveType {
        case add, update
    }
    
    func saveContact(contact: MyContact, saveType: ContactSaveType = .add) -> (Bool, Error?) {
        guard let contact = contact.storedContact else {
            return (false, nil)
        }
        
        let saveRequest = CNSaveRequest()
        
        switch saveType {
        case .add:
            saveRequest.add(contact, toContainerWithIdentifier: nil)
        case .update:
            saveRequest.update(contact)
        }
        
        do {
            try ContactsService.store.execute(saveRequest)
            return (true, nil)
        } catch {
            print("Saving contact failed, error: \(error.localizedDescription)")
            return(false, error)
        }
    }
    
}
