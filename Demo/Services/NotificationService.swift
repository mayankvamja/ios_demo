//
//  NotificationService.swift
//  Demo
//
//  Created by Mayank Vamja on 19/03/21.
//

import UserNotifications
import UIKit

class NotificationService: NSObject, UNUserNotificationCenterDelegate {
    
    static let center = UNUserNotificationCenter.current()
    
    var window: UIWindow?
    init(_ window: UIWindow?) {
        super.init()
        self.window = window
        NotificationService.center.delegate = self
    }
    
    static func requestNotificationAuthorization() {
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (allowed, error) in
            
            if allowed {
                print("Allowed Notifications")
            }
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("-----Notification Clicked")
        
        switch response.notification.request.content.categoryIdentifier {
        case "CDTodoItemNotifications":
            self.handleTodoItemNotifications(response)
            break
            
        default:
            return
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Recived notification when app is running in foreground.")
        completionHandler([.banner, .list, .badge, .sound])
    }
}

// MARK: CDTodoItemNotification
extension NotificationService {
    func handleTodoItemNotifications(_ response: UNNotificationResponse) {
        let userInfo = response.notification.request.content.userInfo
        let listName = userInfo["CDTodoListItem"]
        let index = userInfo["CDTodoItemId"]
        
        if  let main = self.window?.rootViewController as? UINavigationController,
            let vc = UIStoryboard(name: "CDTodo", bundle: nil).instantiateViewController(identifier: "CDTodoListController") as? CDTodoListController {
            
            vc.selectedListName = listName as? String
            vc.indexFromNotification = index as? Int
            
//            _ = self.window?.presentedViewController
            
            main.popToRootViewController(animated: true)
            main.pushViewController(vc, animated: false)
            print("navigating")
            
        }
    }
}
