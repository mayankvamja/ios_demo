//
//  APIViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import UIKit

class APIViewController: UIViewController {

    let http = Http()
    @IBOutlet var loadingActivityIndicator: UIActivityIndicatorView!
    var users: [User] = [] {
        willSet {
            if self.users.count != newValue.count {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUsers()
    }
    
    func loadUsers() {
        self.toggleIndicator(hidden: false)
        http.makeRequest(from: "https://reqres.in/api/users", parameters: ["page": "1", "per_page": "12"], method: "GET") { [weak self] (data, response, error) in
            self?.toggleIndicator()
            if let error = error {
                self?.showMessage(title: "Error", message: error.localizedDescription)
                return
            }

            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode >= 200 && httpResponse.statusCode < 300, let data = data {
                
                if let result = try? JSONDecoder().decode(UserListResponse.self, from: data) {
                    self?.users = result.data
                } else {
                    self?.showMessage(title: "Error", message: "Invalid data received.")
                }
                return
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 404 {
                self?.showMessage(title: "Error", message: "Can not find user.")
            }
        }
        
    }
    
    @IBAction func onAddButtonClicked(_ sender: UIBarButtonItem) {
        
        var firstNameField: UITextField?
        var lastNameField: UITextField?
        var emailField: UITextField?
        
        let alert = UIAlertController(title: "Add new user", message: "", preferredStyle: .alert)
        
        alert.addTextField{ textField in
            textField.placeholder = "First Name"
            firstNameField = textField
        }
        alert.addTextField{ textField in
            textField.placeholder = "Last Name"
            lastNameField = textField
        }
        alert.addTextField{ textField in
            textField.placeholder = "Email"
            emailField = textField
        }
        
        alert.addAction(UIAlertAction(title: "Add", style: .default) { action in
            if let firstname = firstNameField?.text, let lastname = lastNameField?.text, let email = emailField?.text {
                let newUser = User(firstName: firstname, lastName: lastname, email: email)
                let encoder = JSONEncoder()
                
                do {
                    self.loadingActivityIndicator.isHidden = false
                    let body = try encoder.encode(newUser)
                    self.http.makeRequest(from: "https://reqres.in/api/users", body: body) { [weak self] (data, response, error) in
                        self?.loadingActivityIndicator.isHidden = true
                        if let error = error {
                            self?.showMessage(title: "Error", message: error.localizedDescription)
                            return
                        }
                        
                        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode >= 200 && httpResponse.statusCode < 300, let data = data {
                            
                            let decoder = JSONDecoder()
                            decoder.dateDecodingStrategy = .iso8601
                            
                            if let result = try? decoder.decode(NewUserResponse.self, from: data) {
                                self?.users.append(result.toUser())
                            } else {
                                print("Unable to decode")
                            }
                        }
                    }
                } catch {
                    print("Encoding Error ", error.localizedDescription)
                }
                
            }
        })
        
        present(alert, animated: true, completion: nil)
    }
}


extension APIViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  self.tableView.dequeueReusableCell(withIdentifier: "user_list_cell") as! UserListCell
        let user = users[indexPath.row]
        cell.avatarImageView.load(from: user.avatar)
        cell.nameLabel.text = user.firstName + " " + user.lastName
        cell.idLabel.text = "\(user.id ?? 0)"
        cell.emailLabel.text = user.email
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        let user = users[indexPath.row]
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "Options", children: [
                UIAction(title: "Delete") { [weak self] _ in
                    self?.toggleIndicator(hidden: false)
                    self?.http.makeRequest(from: "https://reqres.in/api/users/\(user.id!)", method: "DELETE") {
                        data, response, error in
                        self?.toggleIndicator()
                        if let error = error {
                            self?.showMessage(title: "Error", message: error.localizedDescription)
                            return
                        }
                        
                        if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode >= 200 && httpResponse.statusCode < 300, let data = data {
                            self?.users.remove(at: indexPath.row)
                        }
                    }
                },
                UIAction(title: "Edit") { _ in }
            ])
        }
    }
}

extension UIImageView {
    func load(from urlString: String) {
        if let url = URL(string: urlString) {
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self?.image = image
                        }
                    }
                }
            }
        }
    }
}

extension APIViewController {
    
    func toggleIndicator(hidden: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            self?.loadingActivityIndicator.isHidden = hidden
        }
    }
    
    func showMessage(title: String, message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
