//
//  APIRequestResponse.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import Foundation

struct UserListResponse: Decodable {
    var page: Int
    var perPage: Int
    var total: Int
    var totalPages: Int
    var data: [User]

    enum CodingKeys: String, CodingKey {
      case page
      case perPage = "per_page"
      case total
      case totalPages = "total_pages"
      case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.page = try container.decode(Int.self, forKey: .page)
        self.total = try container.decode(Int.self, forKey: .total)
        self.perPage = try container.decode(Int.self, forKey: .perPage)
        self.totalPages = try container.decode(Int.self, forKey: .totalPages)
        self.data = try container.decodeIfPresent([User].self, forKey: .data) ?? []
     }
}

struct User: Codable {
    var firstName: String
    var lastName: String
    var id: Int?
    var email: String
    var avatar: String
    var createdAt: Date?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case id
        case email
        case avatar
        case createdAt
    }
    
    init() {
        self.id = nil
        self.firstName = ""
        self.lastName = ""
        self.email = ""
        self.avatar = ""
    }
    
    init(firstName: String, lastName: String, email: String, avatar: String = "") {
        self.id = 0
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.avatar = avatar
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let firstName = try container.decode(String.self, forKey: .firstName)
        print(firstName)
        self.firstName = firstName
        self.lastName = try container.decode(String.self, forKey: .lastName)
        self.email = try container.decode(String.self, forKey: .email)
        self.avatar = try container.decode(String.self, forKey: .avatar)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? nil
        self.createdAt = try container.decodeIfPresent(Date.self, forKey: .createdAt) ?? Date()
//        self.createdAt = Date()
     }
    
}

struct NewUserResponse: Decodable {
    var firstName: String
    var lastName: String
    var id: String
    var email: String
    var avatar: String
    var createdAt: String
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case id
        case email
        case avatar
        case createdAt
    }
    
    func toUser() -> User {
        var user = User()
        user.firstName = self.firstName
        user.lastName = self.lastName
        user.id = Int(self.id)
        user.email = self.email
        user.avatar = self.avatar
        return user
    }
}
