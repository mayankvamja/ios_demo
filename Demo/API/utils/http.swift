//
//  http.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import Foundation

struct Http {
    
    var session: URLSession
    
    init(background: Bool = false) {
        if background {
            let config = URLSessionConfiguration.background(withIdentifier: "com.example.demo")
            session = URLSession(configuration: config)
        }
        session = URLSession.shared
    }
    
    func get(from url: String, parameters: [String: String]? = nil, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        let request = URLRequest(url: getUrl(from: url, parameters: parameters))
        
        self.startDataTask(request, completion: completion)

    }
    
    func makeRequest(from url: String, parameters: [String: String]? = nil, method: String = "post", body: Data? = nil, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        var request = URLRequest(url: getUrl(from: url, parameters: parameters))

        request.httpMethod = method
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        self.startDataTask(request, completion: completion)

    }
    
    func startDataTask(_ request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = session.dataTask(with: request, completionHandler: completion)
        task.resume()
    }
    
    func getUrl(from url: String, parameters: [String: String]?) -> URL {
        var components = URLComponents(string: url)!
        if let parameters = parameters {
            components.queryItems = parameters.map { (key, value) in
                URLQueryItem(name: key, value: value)
            }
        }
        return components.url!
    }
    
}


//extension URLRequest {
//    init(_ url: URL) {
//        self.url = url
//        if url.absoluteString.contains("http://keep-plus.herokuapp.com/notes") {
//            self.setValue("", forHTTPHeaderField: "Authorization")
//        }
//    }
//}
