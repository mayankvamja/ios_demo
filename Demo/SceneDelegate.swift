//
//  SceneDelegate.swift
//  Demo
//
//  Created by Mayank Vamja on 01/03/21.
//

import UIKit
import UserNotifications

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var notificationService: NotificationService?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        //print(#function)
        self.notificationService = NotificationService(self.window)
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        //print(#function)
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        //print(#function)
    }

    func sceneWillResignActive(_ scene: UIScene) {
        //print(#function)
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        //print(#function)
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        //print(#function)
    }

}
