//
//  ViewController.swift
//  BMI Calculator
//
//  Created by Mayank Vamja
//

import UIKit

class CalculateViewController: UIViewController {

    @IBOutlet var heightLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var heightSlider: UISlider!
    @IBOutlet var weightSlider: UISlider!
    
    var bmi: BMI = BMI()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    @IBAction func gotoResultVC() {
        
        let weight = weightSlider.value
        let height = heightSlider.value
        bmi.calcuateBmi(height: height, weight: weight)
        
        // Route next page
        self.performSegue(withIdentifier: "gotoResultVC", sender: self)
    }
    
    @IBAction func onHeightChanged(_ sender: UISlider) {
        heightLabel.text = String(format: "%.2f m", sender.value)
    }
    
    @IBAction func onWeightChanged(_ sender: UISlider) {
        weightLabel.text = String(format: "%.2f kg", sender.value)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoResultVC" {
            let destinationVC = segue.destination as? ResultViewController
            destinationVC?.bmi = bmi
        }
    }
}

