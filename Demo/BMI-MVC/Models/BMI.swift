//
//  BMI.swift
//  BMI Calculator
//
//  Created by Mayank Vamja on 03/03/21.
//  Copyright © 2021 Angela Yu. All rights reserved.
//
import UIKit

struct BMI {
    var value: Float = 0.0
    var color: UIColor = .blue
    var advice: String = "No advice"
    
    mutating func calcuateBmi(height: Float, weight: Float) {
        
//        print("Height: ", height)
//        print("Weight: ", weight)
        
        self.value = weight/(height * height)
//        print("BMI: ", value)
        
        if self.value < 18.5 {
            advice = "You need to eat more!"
            color = .blue
        } else if value < 25 {
            advice = "Fit as a fiddle!"
            color = .green
        } else {
            advice = "Eat less!"
            color = .red
        }
    }
    
    func getBMI() -> String {
        return String(format: "%.2f", self.value)
    }
}
