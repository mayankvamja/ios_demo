//
//  NotesHomeViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import UIKit

class NotesHomeViewController: UIViewController {
    
    @IBOutlet var tokenLabel: UILabel!
    @IBOutlet var refreshTokenLabel: UILabel!
    let defaults = UserDefaults.standard
    let http = Http()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let token = defaults.value(forKey: "NOTES_AUTH_TOKEN"), let refToken = defaults.value(forKey: "NOTES_REFRESH_TOKEN") {
            tokenLabel.text = "TOKEN: \(token)"
            refreshTokenLabel.text = "REFRESH TOKEN: \(refToken)"
        } else {
//            self.performSegue(withIdentifier: "notes_login_segue", sender: self)
        }
        
        http.makeRequest(from: "", method: "GET") { data, response, error in
            if let error = error  {
                print(error.localizedDescription)
                return
            }
            
            if let response = response as? HTTPURLResponse {
                print(response.statusCode)
                
                if let data = data {
                    let json = try? JSONSerialization.jsonObject(with: data, options: [])
                    print(json ?? "INVALID DATA")
                }
            }
        }
        
        
    }
    

    @IBAction func logoutTapped(_ sender: Any) {
        defaults.removeObject(forKey: "NOTES_AUTH_TOKEN")
        defaults.removeObject(forKey: "NOTES_REFRESH_TOKEN")
        self.dismiss(animated: true, completion: nil)
    }

}
