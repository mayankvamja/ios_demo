//
//  NotesLoginViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import UIKit

class NotesLoginViewController: UIViewController {

    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    var http: Http = Http()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        self.loadingIndicator.isHidden = false
        
        let loginData = [
            "username": usernameField.text ?? "",
            "password": passwordField.text ?? ""
        ]
        
        if let data = try? JSONSerialization.data(withJSONObject: loginData, options: []) {
            http.makeRequest(from: "https://keep-plus.herokuapp.com/login", method: "POST", body: data) { [weak self] (data, response, error) in
                self?.hideIndicator()
                if let error = error {
                    self?.showMessage(title: "Error", message: error.localizedDescription)
                    print(error.localizedDescription)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    
                    switch response.statusCode {
                    
                    case 200...299:
                        print("Successfully logged in")
                        self?.showMessage(title: "Success", message: "Successfully logged in")
                        guard let res = try? JSONDecoder().decode(NotesUserLoginResponse.self, from: data!) else { return }
                        self?.defaults.set(res.jwtToken, forKey: "NOTES_AUTH_TOKEN")
                        self?.defaults.set(res.refreshToken, forKey: "NOTES_REFRESH_TOKEN")
                        self?.gotoHome()
                        return
                        
                    case 400:
                        guard let res = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] else { return }
                        self?.showMessage(title: "Error", message: res["message"] as? String ?? "BAD REQUEST")
                        return
                        
                    default:
                        self?.showMessage(title: "Error", message: "Unable to login")
                        print("Unable to login")
                        return
                    }
                    
                } else {
                    self?.showMessage(title: "Error", message: "Unexpected login error")
                    print("Unexpected login error")
                }
            }
        }
        
    }
    @IBAction func onBackTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NotesLoginViewController {
    
    func gotoHome() {
        DispatchQueue.main.async { [weak self] in
            self?.dismiss(animated: false) { [weak self] in
                self?.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func hideIndicator() {
        DispatchQueue.main.async { [weak self] in
            self?.loadingIndicator.isHidden = true
        }
    }
    func showMessage(title: String, message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
