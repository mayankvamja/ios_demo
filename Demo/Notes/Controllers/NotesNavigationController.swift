//
//  NotesNavigationController.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import UIKit

class NotesNavigationController: UINavigationController {
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()

        if defaults.value(forKey: "NOTES_AUTH_TOKEN") != nil {
            self.performSegue(withIdentifier: "notes_home_segue", sender: self)
        } else {
            self.performSegue(withIdentifier: "notes_login_segue", sender: self)
        }
        
    }

}
