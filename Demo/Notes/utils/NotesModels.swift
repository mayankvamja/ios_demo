//
//  NotesModels.swift
//  Demo
//
//  Created by Mayank Vamja on 15/03/21.
//

import Foundation

struct NotesUserLoginResponse: Decodable {
    var id: String
    var firstName: String
    var lastName: String
    var username: String
    var dp: String
    var role: String
    var jwtToken: String
    var refreshToken: String
}

struct NotesUserSignupRequest: Encodable {
    var firstName: String
    var lastName: String
    var username: String
    var password: String
}

struct NotesUserSignupResponse: Decodable {
    var success: Bool
    var message: String
}
