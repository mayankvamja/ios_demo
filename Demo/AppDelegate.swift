//
//  AppDelegate.swift
//  Demo
//
//  Created by Mayank Vamja on 01/03/21.
//

import UIKit
import CoreData

//@main
//class FakeApp {
//    static func main() {
//        print("Fake app main func")
//    }
//}

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var backgroundSessionCompletionHandler: (() -> Void)?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NSLog("Launch options %@", launchOptions ?? "")
        print("LAUNCH OPTIONS", launchOptions?.count ?? 0)
        
        ContactsService.requestPermission()
        NotificationService.requestNotificationAuthorization()
        //LocationService.shared.startMySignificantLocationChanges()

        return true
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        //        print(#function)
        backgroundSessionCompletionHandler = completionHandler
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        //print(#function)
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        //print(#function)
    }
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TodosData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
}
