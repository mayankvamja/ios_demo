//
//  ImagePickerViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit
import MessageUI

class ImagePickerViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet var headerLabell: UILabel!
    @IBOutlet var imageView: UIImageView!
    let pickerController = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerController.delegate = self
        
//        print(MFMailComposeViewController.canSendMail())
//        print(MFMessageComposeViewController.canSendText())
        
//        print(UIImagePickerController.availableMediaTypes(for: .photoLibrary))
//        print(UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum))
//        print(UIImagePickerController.availableMediaTypes(for: .camera))
    }
    
    @IBAction func captureImageTapped() {
        
        if(UIImagePickerController.isSourceTypeAvailable(.camera)) {
            
            pickerController.allowsEditing = true
            pickerController.mediaTypes = ["public.image"]
            pickerController.sourceType = .camera
            present(pickerController, animated: true)
            
        } else {
            
            self.showToast(message: "Device does not support camera", seconds: 1.0)
        }
    }
  
    @IBAction func pickImageTapped() {
        
        pickerController.sourceType = .photoLibrary
        pickerController.mediaTypes = ["public.image"]
        pickerController.allowsEditing = true
        
        present(pickerController, animated: true)
        
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

}

//MARK: UIImagePickerControllerDelegate
extension ImagePickerViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage {
            self.imageView.image = image
            dismiss(animated: true)
            return
        }
        
        dismiss(animated: true)
    }
}
