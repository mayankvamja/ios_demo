//
//  TodosData.swift
//  Demo
//
//  Created by Mayank Vamja on 09/03/21.
//

struct TodosData {
    var data: [TodoList] = []
    
    mutating func add(from name: String) {
        data.append(TodoList(name:  name))
    }
}
