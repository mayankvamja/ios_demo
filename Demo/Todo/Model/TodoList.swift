//
//  TodoList.swift
//  Demo
//
//  Created by Mayank Vamja on 07/03/21.
//

import UIKit
import MobileCoreServices

class TodoList {
    
    var name: String = ""
    var items: [TodoItem] = []
    
    init(name: String = "" , list: [TodoItem] = []) {
        self.name = name
        self.items = list
    }
    
//    init() {
//        self.items = [
//            TodoItem(text: "Tdod item 1", id: UUID().uuidString),
//            TodoItem(text: "Tdod item 2", id: UUID().uuidString),
//            TodoItem(text: "Tdod item 3", id: UUID().uuidString),
//            TodoItem(text: "Tdod item 4", id: UUID().uuidString),
//            TodoItem(text: "Tdod item 5", id: UUID().uuidString),
//        ]
//    }
    
    func add(_ text: String, date: Date? = nil) {
        print("ADD TODO CALLED")
//        let x = TodoItem(
        let newTodo = TodoItem(text: text, id: UUID().uuidString, date: date)
        self.items.append(newTodo)
    }
    
    func update(with id: String, text: String, date: Date?) {
        if let index = self.items.firstIndex(where: { item in item.id == id }) {
            self.items[index].text = text
            self.items[index].date = date
        }
    }

    func delete(with id: String) {
        self.items = self.items.filter{ item in
            item.id != id
        }
    }
    
    func clearAll() {
        self.items = []
    }
    
    func changeStatus(with id: String) {
        if let index = self.items.firstIndex(where: { item in item.id == id }) {
            self.items[index].done = !self.items[index].done
        }
    }
    
    func moveItem(from: Int, to: Int) {
        guard from != to else { return }
        
        let todo = items[from]
        items.remove(at: from)
        items.insert(todo, at: to)
    }
    
    func dragItems(for indexPath: IndexPath) -> [UIDragItem] {
        
        guard let data = items[indexPath.row].text.data(using: .utf8) else { return [] }
        
        let itemProvider = NSItemProvider(item: data as NSData, typeIdentifier: kUTTypePlainText as String)

        return [UIDragItem(itemProvider: itemProvider)]
    }
}
