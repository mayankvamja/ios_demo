//
//  TodoItem.swift
//  Demo
//
//  Created by Mayank Vamja on 07/03/21.
//

import Foundation

struct TodoItem {
    var text: String = ""
    var id: String = "1"
    var date: Date? = nil
    var done: Bool = false
}
