//
//  ViewController.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class TodosHomeController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func onAddButtonClicked(_ sender: Any) {
        
        var textField: UITextField!
        
        let alert = UIAlertController(title: "Add new todo list", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Item", style: .cancel) { action in
            print(textField.text ?? "")
            print("Added new item")
        }
        
        alert.addTextField { alertTextField in
            alertTextField.placeholder = "Type todo list name"
            textField = alertTextField
        }
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onBackPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

