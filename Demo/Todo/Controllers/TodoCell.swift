//
//  TodoCell.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class TodoCell: UITableViewCell {

    @IBOutlet var todoTextLabel: UILabel!
    @IBOutlet var todoCheckButton: UIButton!
    @IBOutlet var todoEditButton: UIButton!
    @IBOutlet var todoDateLabel: UILabel!
    var todo: TodoItem?
    var statusChangeDelegate: (String) -> Void = { _ in }
    
    func configure(with todo: TodoItem) {
        todoTextLabel.text = todo.text.replacingOccurrences(of: "\n", with: " ")
        setDateLabel(from: todo.date)
        todoCheckButton.setImage(getCheckButtonImage(todo.done), for: .normal)
        
        self.todo = todo
    }
    
    func setDateLabel(from date: Date?) {
        if let date = date {
            let df = DateFormatter()
            df.dateFormat = "MMM d, y 'at' hh:mm aa"
            todoDateLabel.text = df.string(from: date)
            return
        }
        todoDateLabel.text = nil
    }
    
    func getCheckButtonImage(_ done: Bool) -> UIImage? {
        return done
            ?UIImage(systemName: "square")
            : UIImage(systemName: "checkmark.square.fill")
    }
    
    @IBAction func checkButtonClicked() {
        todoCheckButton.setImage(getCheckButtonImage(!todo!.done), for: .normal)
        self.statusChangeDelegate(self.todo!.id)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
