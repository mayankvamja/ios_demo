//
//  TodoListController+Drag+Drop.swift
//  Demo
//
//  Created by Mayank Vamja on 08/03/21.
//

import UIKit

extension TodoListController: UITableViewDragDelegate {
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        todoList.moveItem(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    // MARK: UITableViewDragDelegate
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        return todoList.dragItems(for: indexPath)
    }
    
    /* // MARK: UITableViewDropDelegate
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        print("PERFORM DROP")
    } */
}
