//
//  TodoListController+Data.swift
//  Demo
//
//  Created by Mayank Vamja on 08/03/21.
//

import UIKit

extension TodoListController {
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Table DATA LOADING")
        return todoList.items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "todo_item", for: indexPath) as! TodoCell

        cell.configure(with: todoList.items[indexPath.row])
        cell.statusChangeDelegate = { [unowned self] (id: String) in
            self.todoList.changeStatus(with: id)
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedTodo = todoList.items[indexPath.row]
        self.performSegue(withIdentifier: "update_todo_segue", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    func makeSwipeToDeleteConfiguration(_ indexPath: IndexPath) -> UISwipeActionsConfiguration {
        
        let id = todoList.items[indexPath.row].id
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            
            self.todoList.delete(with: id)
            self.tableView.deleteRows(at: [indexPath], with: .top)
            
            completionHandler(true)
        }
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        return .insert
//    }
    
//    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
//        return false
//    }
}
