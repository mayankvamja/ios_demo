//
//  TodoListControllerTableViewController.swift
//  Todo Plus
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class TodoListController: UITableViewController {
    
    var selectedTodo: TodoItem?
    var todoList: TodoList = TodoList()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dragInteractionEnabled = true
        tableView.dragDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(todoList.items.count)
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "update_todo_segue":
            if let updateTodoVC = segue.destination as? ViewTodoController {
                updateTodoVC.todoList = self.todoList
                updateTodoVC.configureAsUpdateView(selectedTodo)
                selectedTodo = nil
            }
            
        case "add_todo_segue":
            if let addTodoVC = segue.destination as? ViewTodoController {
                addTodoVC.todoList = self.todoList
            }
            
        default:
            return
        }
    }
    
}
