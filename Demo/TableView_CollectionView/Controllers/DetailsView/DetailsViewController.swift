//
//  DetailsViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var receivedText: String = "Details"

    override func viewDidLoad() {
        super.viewDidLoad()

        title = receivedText
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
