//
//  MyCollectionViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class MyCollectionViewController: UICollectionViewController {
    
    let dataList = [
        [(label: "Home", image: UIImage(systemName: "homepod.2.fill")),
        (label: "Mail", image: UIImage(systemName: "mail.fill")),
        (label: "Book", image: UIImage(systemName: "book.circle.fill")),
        (label: "Camera", image: UIImage(systemName: "camera.fill")),
        (label: "Alarm", image: UIImage(systemName: "alarm")!),
        (label: "Home", image: UIImage(systemName: "homepod.2.fill")),
        (label: "Mail", image: UIImage(systemName: "mail.fill")),
        (label: "Book", image: UIImage(systemName: "book.circle.fill")),
        (label: "Camera", image: UIImage(systemName: "camera.fill")),
        (label: "Alarm", image: UIImage(systemName: "alarm")!),
        (label: "Home", image: UIImage(systemName: "homepod.2.fill")),
        (label: "Mail", image: UIImage(systemName: "mail.fill")),
        (label: "Book", image: UIImage(systemName: "book.circle.fill")),
        (label: "Camera", image: UIImage(systemName: "camera.fill")),
        (label: "Alarm", image: UIImage(systemName: "alarm")!),
        (label: "Home", image: UIImage(systemName: "homepod.2.fill")),
        (label: "Mail", image: UIImage(systemName: "mail.fill"))],
        
        [(label: "Book", image: UIImage(systemName: "book.circle.fill")),
        (label: "Camera", image: UIImage(systemName: "camera.fill")),
        (label: "Alarm", image: UIImage(systemName: "alarm")!)]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataList[section].count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "details_cell", for: indexPath) as! DetailsCollectionCell
        
        let item = dataList[indexPath.section][indexPath.row]
        cell.configure(with: item.label, image: item.image)
    
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected Item : \(dataList[indexPath.section][indexPath.row])")
    }

    
}

extension MyCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.frame.size.width
        let size = screenWidth > 450 ? (screenWidth - 30) / 4 : screenWidth > 300 ? (screenWidth - 20) / 3 : (screenWidth - 10) / 2
        print("SIZE: ", size)
        return CGSize(width: size, height: size)
    }
    
}
