//
//  DetailsCollectionCell.swift
//  Demo
//
//  Created by Mayank Vamja on 05/03/21.
//

import UIKit

class DetailsCollectionCell: UICollectionViewCell {
    @IBOutlet var image: UIImageView!
    
    @IBOutlet var label: UILabel!
    
    func configure(with label: String, image source: UIImage?) {
        self.image.image = source
        self.label.text = label
    }
}
