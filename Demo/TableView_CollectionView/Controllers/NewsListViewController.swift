//
//  NewsListViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 04/03/21.
//

import UIKit

class NewsListViewController: UIViewController {
    
    var selectedItem: String?
    
    @IBOutlet var tableView: UITableView!
    
    let newsList: [[String]] = [
        ["News One", "Lorem Ipsum", "Asad Kasda", "YQabs Jasas"],
        ["HGasdna aJjasnd", "bZBwnsn kNKAS"],
        ["GSbda Ja da", "Lans JAsska"]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue" {
            guard let vc = segue.destination as? DetailsViewController else {
                return
            }
            vc.receivedText = selectedItem ?? "Details"
        }
    }

}

extension NewsListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section - \(section + 1)"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "table_item", for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "table_item_with_icon", for: indexPath)
        }
        
        cell.textLabel?.text = newsList[indexPath.section][indexPath.row]
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected Row of section: \(indexPath.section) : at position : ", indexPath.row)
        
        selectedItem = newsList[indexPath.section][indexPath.row]
        
        self.performSegue(withIdentifier: "detailsSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            let moreOptions = UIMenu(title: "Edit >", children: [
                UIAction(title: "Copy") { _ in },
                UIAction(title: "Cut") { _ in }
            ])
            return UIMenu(title: "Title", children: [
                UIAction(title: "Share") { _ in },
                moreOptions
            ])
        }
    }
    
}


