//
//  ViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 01/03/21.
//

import UIKit

class TC_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func gotoNextPage() {
        self.performSegue(withIdentifier: "NewsCategorySegue", sender: self)
    }
    
    @IBAction func unwindToOne(_ segue: UIStoryboardSegue) {}
    
}

