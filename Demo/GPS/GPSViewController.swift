//
//  GPSViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 18/03/21.
//

import UIKit
import WebKit
import CoreLocation

class GPSViewController: UIViewController {

    @IBOutlet var button: UIButton!
    @IBOutlet var webView: WKWebView!
    let locationManager = CLLocationManager()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
//        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        
    }
    

    @IBAction func getMyLocation() {
        switch locationManager.authorizationStatus {
        case .denied, .notDetermined, .restricted:
            print("Does not have permission")
            locationManager.requestAlwaysAuthorization()
            
        default:
            print("App has permission to go")
        }
//        locationManager.startUpdatingLocation()
        locationManager.requestLocation()
    }
}

extension GPSViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations.count)
        if let location = locations.last {

            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            
            if !webView.isLoading {
                let mapUrl = "http://maps.google.com/maps?q=\(latitude),\(longitude)"
                let request = URLRequest(url: URL(string: mapUrl)!)
                webView.load(request)
            }
        }
        
    }
}
