//
//  AppMainViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 11/03/21.
//

import UIKit
//import UserNotifications

class AppMainViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        UNUserNotificationCenter.current().delegate = self
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        print("View Will Appear")
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        print("View Did Appear")
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        print("View Will Disappear")
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        print("View Did Disappear")
//    }
//
//    override func viewWillLayoutSubviews() {
//        print("view will layout subviews")
//    }
//
//    override func viewDidLayoutSubviews() {
//        print("view did layout subviews")
//    }
//
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        print("view will resize")
//    }

}


/*
extension AppMainViewController: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("-----Notification Clicked")
        
        switch response.notification.request.content.categoryIdentifier {
        case "CDTodoItemNotifications":
            let userInfo = response.notification.request.content.userInfo
            let listName = userInfo["CDTodoListItem"]
            let index = userInfo["CDTodoItemId"]
            
            if let vc = UIStoryboard(name: "CDTodo", bundle: nil).instantiateViewController(identifier: "CDTodoListController") as? CDTodoListController {
                vc.selectedListName = listName as? String
                vc.indexFromNotification = index as? Int
                self.navigationController?.show(vc, sender: self)
            }
            break
            
        default:
            return
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Recived notification when app is running in foreground.")
        completionHandler([.banner, .badge, .sound])
    }
}
*/
