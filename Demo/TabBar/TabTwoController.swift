//
//  TabTwoController.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit

class TabTwoController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    var observer: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("TaabTwo Loaded")
        
        self.observer = NotificationCenter.default.addObserver(forName: .nameChangeNotification, object: nil, queue: OperationQueue.main) { (notification) in
            let name = notification.object as? String ?? "Unknown"
            self.nameLabel.text = name
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(observer!)
    }

}
