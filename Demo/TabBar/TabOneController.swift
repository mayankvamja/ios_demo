//
//  TabOneController.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit

class TabOneController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("TabOne Loaded")
    }
  
    @IBAction func nameChnaged(_ sender: UITextField) {
        NotificationCenter.default.post(name: .nameChangeNotification, object: sender.text)
    }
}
