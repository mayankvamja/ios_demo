//
//  NotificationNames.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//
import UIKit

extension Notification.Name {
    static let nameChangeNotification  = Notification.Name("com.example.demo.NameChangeNotification")
}
