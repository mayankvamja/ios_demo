import AVFoundation
import AVKit
import UIKit

class ITunesViewController: UIViewController {
    
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    let downloadService = ITunesDownloadService()
    let queryService = ITunesSearchService()
    
    var searchWorkItem: DispatchWorkItem? = nil
    var searchGroup: DispatchGroup = DispatchGroup()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    lazy var downloadsSession: URLSession = URLSession(
        configuration: URLSessionConfiguration
            .background(withIdentifier: "com.example.demo.tunesBgSession"),
        delegate: self,
        delegateQueue: nil)
    
    var searchResults: [Track] = []
    
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    
    func playDownload(_ track: Track) {
        let playerViewController = AVPlayerViewController()
        present(playerViewController, animated: true, completion: nil)
        
        let url = localFilePath(for: track.previewURL)
        let player = AVPlayer(url: url)
        playerViewController.player = player
        player.play()
    }
    
    func reload(_ row: Int) {
        tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadService.downloadsSession = downloadsSession
        startSearchTask()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

// MARK: - Search Bar Delegate
extension ITunesViewController: UISearchBarDelegate {
    
    func startSearchTask(_ text: String = "a") {
        searchWorkItem?.cancel()
        
        searchWorkItem = DispatchWorkItem {
            self.queryService.getSearchResults(searchTerm: text) { [weak self] results, errorMessage in
                guard let self = self else { return }
                
                if let results = results {
                    self.searchResults = results
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                
                if !errorMessage.isEmpty {
                    print(errorMessage)
                    self.showMessage(title: "Search error", message: errorMessage)
                }
            }
        }
        
//        DispatchQueue.main.async(execute: searchWorkItem!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: searchWorkItem!)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchBar.text, !searchText.isEmpty else {
            self.startSearchTask()
            return
        }
        
        self.startSearchTask(searchText)
    
    }
}

extension ITunesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TrackCell = tableView.dequeueReusableCell(withIdentifier: TrackCell.identifier, for: indexPath) as! TrackCell
        
        cell.delegate = self
        
        let track = searchResults[indexPath.row]
        cell.configure(track: track,
                       downloaded: track.downloaded,
                       download: downloadService.activeDownloads[track.previewURL])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
}

extension ITunesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let track = searchResults[indexPath.row]
        if track.downloaded {
            playDownload(track)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62.0
    }
}

// MARK: - Track Cell Delegate
extension ITunesViewController: TrackCellDelegate {
    func cancelTapped(_ cell: TrackCell) {
        if let (track, index) = getTractInfo(cell) {
            downloadService.cancelDownload(track)
            reload(index)
        }
    }
    
    func downloadTapped(_ cell: TrackCell) {
        if let (track, index) = getTractInfo(cell) {
            downloadService.startDownload(track)
            reload(index)
        }
    }
    
    func pauseTapped(_ cell: TrackCell) {
        if let (track, index) = getTractInfo(cell) {
            downloadService.pauseDownload(track)
            reload(index)
        }
    }
    
    func resumeTapped(_ cell: TrackCell) {
        if let (track, index) = getTractInfo(cell) {
            downloadService.resumeDownload(track)
            reload(index)
        }
    }
    
    func getTractInfo(_ cell: TrackCell) -> (Track, Int)? {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = searchResults[indexPath.row]
            return (track, indexPath.row)
        }
        return nil
    }
}

// MARK: - URL Session Delegate
extension ITunesViewController: URLSessionDelegate {
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
               let completionHandler = appDelegate.backgroundSessionCompletionHandler {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }
}

// MARK: - URL Session Download Delegate
extension ITunesViewController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        
        guard let sourceURL = downloadTask.originalRequest?.url else {
            return
        }
        
        let download = downloadService.activeDownloads[sourceURL]
        downloadService.activeDownloads[sourceURL] = nil
        
        let destinationURL = localFilePath(for: sourceURL)
        print(destinationURL)
        
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        
        do {
            print(location)
            try fileManager.copyItem(at: location, to: destinationURL)
            download?.track.downloaded = true
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        
        if let index = download?.track.index {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
        guard
            let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url] else {
            return
        }
        
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: .file)
        
        DispatchQueue.main.async {
            if let trackCell = self.tableView.cellForRow(at: IndexPath(row: download.track.index, section: 0)) as? TrackCell {
                trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
            }
        }
    }
}

extension ITunesViewController {
    func showMessage(title: String, message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
