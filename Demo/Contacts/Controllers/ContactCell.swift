//
//  ContactCell.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit
import Contacts

class ContactCell: UITableViewCell {
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var numberLabel: UILabel!
    @IBOutlet private var profileImageView: UIImageView! {
        didSet {
            profileImageView.layer.masksToBounds = true
            profileImageView.layer.cornerRadius = 22.0
        }
    }
    
    var contact : MyContact? {
        didSet {
            configure()
        }
    }
    
    private func configure() {
        let contact = self.contact!
        nameLabel.text = contact.firstName + " " + contact.lastName
        numberLabel.text = contact.phoneNumber
        if let image = contact.profilePicture {
            profileImageView.image = image
        }
    }
}
