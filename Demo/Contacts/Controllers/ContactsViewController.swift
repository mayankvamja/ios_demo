//
//  ContactsViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit
import Contacts
import ContactsUI

class ContactsViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var contacts: [MyContact] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    let contactsService = ContactsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.global(qos: .userInitiated).async { [unowned self] in
            self.contacts = self.contactsService.fetchAllContacts()
        }
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}

//MARK: - UITableViewDataSource
extension ContactsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact_cell", for: indexPath)
        
        if let cell = cell as? ContactCell {
            let contact = contacts[indexPath.row]
            cell.contact = contact
        }
        
        return cell
    }
}

//MARK: - UITableViewDelegate
extension ContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let listContact = contacts[indexPath.row]
        let contact = listContact.contactValue
        
        let contactViewController = CNContactViewController(forUnknownContact: contact)
        contactViewController.hidesBottomBarWhenPushed = true
        contactViewController.allowsEditing = false
        contactViewController.allowsActions = false
        
        navigationController?.pushViewController(contactViewController, animated: true)
    }
}
