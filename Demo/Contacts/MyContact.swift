//
//  MyContact.swift
//  Demo
//
//  Created by Mayank Vamja on 22/03/21.
//

import UIKit
import Contacts

class MyContact {
    var firstName: String
    var lastName: String
    var workEmail: String?
    var identifier: String
    var profilePicture: UIImage?
    var phoneNumber: String?
    var storedContact: CNMutableContact?
    var phoneNumberField: (CNLabeledValue<CNPhoneNumber>)?
    
    init(contact: CNContact) {
        let workEmail = contact.emailAddresses.first?.value as String?
        
        self.identifier = contact.identifier
        self.storedContact = contact.mutableCopy() as? CNMutableContact
        self.firstName = contact.givenName
        self.lastName = contact.familyName
        self.workEmail = workEmail
        
        if let imageData = contact.imageData {
            self.profilePicture = UIImage(data: imageData)
        }
        
        if let contactPhone = contact.phoneNumbers.first {
            phoneNumberField = contactPhone
            phoneNumber = contactPhone.value.stringValue
        }
    }
    
}

extension MyContact {
    var contactValue: CNContact {
        let contact = CNMutableContact()
        contact.givenName = firstName
        contact.familyName = lastName
        
        if let email = workEmail as NSString? {
            contact.emailAddresses = [CNLabeledValue(label: CNLabelWork, value: email)]
        }
        
        if let profilePicture = profilePicture {
            let imageData = profilePicture.jpegData(compressionQuality: 1)
            contact.imageData = imageData
        }
        
        if let phoneNumberField = phoneNumberField {
            contact.phoneNumbers.append(phoneNumberField)
        }
        
        return contact.copy() as! CNContact
    }
}
