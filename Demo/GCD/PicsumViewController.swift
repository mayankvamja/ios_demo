//
//  PicsumViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 17/03/21.
//

import UIKit

class PicsumViewController: UIViewController {

    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    var images: [UIImage] = []
    let dummyData = Array(repeating: 0, count: 50)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loadImages()

    }

}

extension PicsumViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return images.count
        return dummyData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "picsum_cell", for: indexPath) as! PicsumCell
        
        if let url = URL(string: "https://picsum.photos/300/300?random=\(indexPath.row+1)") {
            cell.config(with: url)
        }

        /*DispatchQueue.global().async {
            if let url = URL(string: "https://picsum.photos/300/300?random=\(indexPath.row+1)"),
               let data = try? Data(contentsOf: url),
               let image = UIImage(data: data) {
                cell.config(image)
            }
        }*/
        
        //cell.config(images[indexPath.row])

        return cell
    }

    
    func loadImages() {
        
//        let group = DispatchGroup()
//        let queue = DispatchQueue(label: "LoadImagesQueue", attributes: .concurrent)
        let operationQueue = OperationQueue()
        //operationQueue.maxConcurrentOperationCount = 1
        operationQueue.qualityOfService = .userInitiated
        
        for i in 0..<30 {
            
            let urlString = "https://picsum.photos/300/300?random=\(i+1)"
            if let url = URL(string: urlString) {

//                queue.async(group: group) { [weak self] in
//                    guard let self = self else { return }
                    
                let operation = BlockOperation {
                    if let data = try? Data(contentsOf: url) {
                        if let image = UIImage(data: data) {
                            self.images.append(image)
                            
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                            
                        }
                    }
                    
                }
                operationQueue.addOperation(operation)
            }
        }
        
        operationQueue.addBarrierBlock {
            print("All images loaded")
            OperationQueue.main.addOperation {               self.loadingIndicator.stopAnimating()
            }
        }
    }
}

extension PicsumViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.frame.size.width
        let size = screenWidth > 450
            ? (screenWidth - 25) / 4
            : screenWidth > 300
            ? (screenWidth - 20) / 3
            : (screenWidth - 15) / 2
        return CGSize(width: size, height: size)
    }

}
