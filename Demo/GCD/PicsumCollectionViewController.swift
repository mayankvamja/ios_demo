//
//  PicsumCollectionViewController.swift
//  Demo
//
//  Created by Mayank Vamja on 17/03/21.
//

import UIKit
import Kingfisher

private let reuseIdentifier = "picsum_cell"

class PicsumCell: UICollectionViewCell {
    @IBOutlet var img: UIImageView!
    
    func config(_ image: UIImage) {
        DispatchQueue.main.async {
            self.img.image = image
        }
    }
    
    func config(with url: URL) {
        self.img.kf.setImage(with: url)
    }
}

class PicsumCollectionViewController: UICollectionViewController {
    
    var images: [UIImage] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadImages()
        
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PicsumCell
        

        cell.config(images[indexPath.row])

        return cell
    }

    
    func loadImages() {
        
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "LoadImagesQueue", attributes: .concurrent)
        
        for i in 0..<30 {
            
            let urlString = "https://picsum.photos/300/300?random=\(i+1)"
            if let url = URL(string: urlString) {
                queue.async(group: group) { [weak self] in
                    
                    guard let self = self else { return }
                    
                    if let data = try? Data(contentsOf: url) {
                        if let image = UIImage(data: data) {
                            self.images.append(image)
                            
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                            
                        }
                    }
                }
            }
        } // for loop ended
        
        group.notify(queue: DispatchQueue.main) {
            print("All images loaded")
            
            DispatchQueue.main.async {
                
            }
        }
        
    }

}

extension PicsumCollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = collectionView.frame.size.width
        let size = screenWidth > 450 ? (screenWidth - 25) / 4 : screenWidth > 300 ? (screenWidth - 20) / 3 : (screenWidth - 15) / 2
//        print("SIZE: ", size)
        return CGSize(width: size, height: size)
    }

}
